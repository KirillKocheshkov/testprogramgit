﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(TopicDataBase)) ]

public class TopicEditor : Editor 
{
private TopicDataBase creator;

    private void Awake() 
    {
        creator = (TopicDataBase)target;
        
    }
    public override void OnInspectorGUI() {
        
    {
         GUILayout.BeginHorizontal();
         if(GUILayout.Button("<<"))
        {
            creator.PreviousItem();
        }
         if(GUILayout.Button(">>"))
        {
            creator.NextItem();
        }
        GUILayout.EndHorizontal();

       
        GUILayout.Space(20);

        if(GUILayout.Button("New Item"))
        {
           
            creator.AddTopic();
        }

         if(GUILayout.Button("Remove Item"))
        {
            creator.RemoveItem();
        }     
      
     
    
        base.OnInspectorGUI();
        
    }
}
}


  