﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ComfirmManager : MonoBehaviour
{
    [SerializeField] ButtonPressed yesBut;
    [SerializeField] ButtonPressed noBut;

    public ButtonPressed NoBut { get => noBut;  }
    public ButtonPressed YesBut { get => yesBut; }

    private void Start()
    {
        noBut.OnPressedEvent += ResetYesButton;
    }


    private void ResetYesButton()
    {
      YesBut.CLearEvents();
    }
}
