﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediatorInitializer : IScriptInitMediator
{
    public ListContentSript ListContentSript;
    public QuestionManager QuestionManager;
    public ResultsScript ResultsScript;
    public static MediatorInitializer GetInstance { get; private set; }
    private MediatorInitializer()
    {

    }
    static MediatorInitializer()
    {
        GetInstance = new MediatorInitializer();
    }

    public void RequestInitialization(MonoBehaviour component)
    {
        switch (component)
        {
            case ListContentSript list:
            {
              list.Hide();
              QuestionManager.gameObject.SetActive(true);
              QuestionManager.Initialize(list.CurrentToppic);
                break;
            }
            case QuestionManager qM:
            {
                ResultsScript.gameObject.SetActive(true);
                ResultsScript.Init(qM.correctAnswers,qM.CurrentTopic);
                qM.gameObject.SetActive(false);
                break;
            }
            case ResultsScript rS:
            {
                break;
            }
        }
    }


}
