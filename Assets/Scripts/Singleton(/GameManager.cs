﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
   public static GameManager Instance {get; private set;}
     public List<Topic> ListOfTopics { get => listOfTopics; private set => listOfTopics = value; }
    
    

    [SerializeField] private TopicDataBase topicCreator;

    [SerializeField,HideInInspector] private List<Topic> listOfTopics;

    #region UiElements

      [SerializeField] private QuestionManager _questionManager;
      [SerializeField] private ResultsScript _resultsScript;
      
    #endregion
    
    

    public ProfileBase currentProfile;
    
 
   private void Awake()
   {
      Instance = this;
      
      ListOfTopics = topicCreator.TopicList;
      var InitMediator = MediatorInitializer.GetInstance;
      InitMediator.ResultsScript = _resultsScript;
      InitMediator.QuestionManager = _questionManager;
      
      
   }
private void Start()
 {
    

   
}

   
}
