﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionCSVItems
{
    private string imageLink;
    private int questionNumber;
    private string questionText;
    private string firstOption;
    private string firstWrongAnswer;
    private string firstLink;
    private string secondOption;
    private string secondWrongAnswer;
    private string secondLink;
    private string thirdOption;
    private string thirdWrongAnswer;
    private string thirdLink;
    private string fourthOption;
    private string fourthWrongAnswer;
    private string fourthLink;
    private string fifthOption;
    private string fifthWrongAnswer;
    private string fifthLink;
    private int correctNumber;

    
    public string ImageLink { get => imageLink; set => imageLink = value; }

    public int QuestionNumber { get => questionNumber; set => questionNumber = value; }
    public string QuestionText { get => questionText; set => questionText = value; }
    public string FirstOption { get => firstOption; set => firstOption = value; }
    public string FirstWrongAnswer { get => firstWrongAnswer; set => firstWrongAnswer = value; }
    public string FirstLink { get => firstLink; set => firstLink = value; }
    public string SecondOption { get => secondOption; set => secondOption = value; }
    public string SecondWrongAnswer { get => secondWrongAnswer; set => secondWrongAnswer = value; }
    public string SecondLink { get => secondLink; set => secondLink = value; }
    public string ThirdOption { get => thirdOption; set => thirdOption = value; }
    public string ThirdWrongAnswer { get => thirdWrongAnswer; set => thirdWrongAnswer = value; }
    public string ThirdLink { get => thirdLink; set => thirdLink = value; }
    public string FourthOption { get => fourthOption; set => fourthOption = value; }
    public string FourthWrongAnswer { get => fourthWrongAnswer; set => fourthWrongAnswer = value; }
    public string FourthLink { get => fourthLink; set => fourthLink = value; }
    public int CorrectNumber { get => correctNumber; set => correctNumber = value; }
    public string FifthOption { get => fifthOption; set => fifthOption = value; }
    public string FifthWrongAnswer { get => fifthWrongAnswer; set => fifthWrongAnswer = value; }
    public string FifthLink { get => fifthLink; set => fifthLink = value; }
}

