﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LINQtoCSV;
using System.IO;
using System.Linq;

public class CSVParseSystem : ParseSystemMain
{

   private void Awake() 
   {
        loader = GetComponent<ITextLoader>();
   }
    
    // Start is called before the first frame update
    public override List<QuestionItems> GetQuestionsFromText(string fileName)
    {
        var currentText = GetTextFromLoader(loader,fileName );

        return ParseTextFromFile(currentText);
    }
   /* get Text asset file
        get first row and extract question
        then extract options
        return list of struct
   */

    protected override TextAsset GetTextFromLoader(ITextLoader currentLoader, string fileName)
    {
        return currentLoader.LoadTextFile(fileName);
    }
     private List<QuestionItems> ParseTextFromFile(TextAsset currentText)
    {
       var qustionsList = new List<QuestionItems> ();
       CsvFileDescription fileDescription = new CsvFileDescription
       {
           SeparatorChar = ',',
           FirstLineHasColumnNames = true
       };
        CsvContext cc = new CsvContext();
       using(var ms = new MemoryStream())
       {
           using (var writer = new StreamWriter(ms))
           {
               using (var reader = new StreamReader(ms))
               {
                   writer.Write(currentText.text);
                   writer.Flush();
                   ms.Seek(0,SeekOrigin.Begin);
                   cc.Read<QuestionCSVItems>(reader, fileDescription)
                   .ToList()
                   .ForEach( qi => 
                   {
                       QuestionItems lS = new QuestionItems();
                       lS.imageLink = qi.ImageLink;
                       lS.questionID = qi.QuestionNumber;
                       lS.questionText = qi.QuestionText;
                       lS.correctOption = qi.CorrectNumber;
                       lS.optionsList = new List<OptionsItems>();
                       var fOpt = new OptionsItems(qi.FirstOption,qi.FirstWrongAnswer,qi.FirstLink);
                       lS.optionsList.Add(fOpt);
                       var sOpt = new OptionsItems(qi.SecondOption,qi.SecondWrongAnswer,qi.SecondLink);
                       lS.optionsList.Add(sOpt);
                       var tOpt = new OptionsItems (qi.ThirdOption,qi.ThirdWrongAnswer,qi.ThirdLink);
                       lS.optionsList.Add(tOpt);
                       var frOpt = new OptionsItems (qi.FourthOption,qi.FourthWrongAnswer,qi.FourthLink);
                       lS.optionsList.Add(frOpt);
                       var fifth = new OptionsItems (qi.FifthOption,qi.FifthWrongAnswer,qi.FifthLink);
                       lS.optionsList.Add(fifth);
                       qustionsList.Add(lS);
                   });
               }
           }
       }
       return qustionsList;
       
    }
    
}
