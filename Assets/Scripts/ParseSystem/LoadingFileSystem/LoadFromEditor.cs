﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadFromEditor : MonoBehaviour, ITextLoader
{
    public TextAsset LoadTextFile(string name)
    {
       TextAsset currentAsset = Resources.Load<TextAsset>("CSV/"+name);
       return currentAsset;
    }

   
}
