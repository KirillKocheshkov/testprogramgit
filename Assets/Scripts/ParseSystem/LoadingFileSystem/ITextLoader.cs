﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITextLoader
{
     TextAsset LoadTextFile(string name); 
}  