﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class ParseSystemMain : MonoBehaviour
{

    [SerializeField] protected ITextLoader loader;

    public ITextLoader Loader { get => loader; protected set => loader = value; }

    protected abstract  TextAsset GetTextFromLoader(ITextLoader currentLoader, string fileName);
    public abstract List<QuestionItems> GetQuestionsFromText (string fileName);
    
}
