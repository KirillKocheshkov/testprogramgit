﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTestScript : MonoBehaviour
{
[SerializeField] QuestionManager questionManager;
ButtonPressed pressed;

private void Awake() 
{
    pressed = GetComponent <ButtonPressed>();
}
    public void OnrandomPressed()
    {
         List<Topic> topics = GameManager.Instance.ListOfTopics;
          var currentTopic = topics[UnityEngine.Random.Range(0,topics.Count -1)];
           pressed.OnButtonPressed();     
          questionManager.Initialize(currentTopic);
          
    }
}
