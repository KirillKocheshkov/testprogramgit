﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour, IInitializer
{
    #region ("UI elements" )
    // Start is called before the first frame update
    [Header ( " Ui Elements"), SerializeField] 
    Text header;
    [SerializeField]
    Image image; 
     [ SerializeField]
    Text question; 
     [ SerializeField]
    OnOptionsPressed[] opttions;
     
    

    
    #endregion

    #region ("Components" )

     
    private ParseSystemMain currentParseSystem;
    private TopicFabric currentFabric;
    private IImageLoader imageLoader; // create class;

     #endregion

   #region ("Links" )
   private Topic currentTopic;
      
   private List<Question> currentList;
   public Dictionary <Question, bool> correctAnswers;
   private Question currentQuestion;
   public IScriptInitMediator Mediator {get; private set;}
   


   

#endregion
    public Question CurrentQuestion { get => currentQuestion;private set => currentQuestion = value; }
    public Topic CurrentTopic { get => currentTopic; private set => currentTopic = value; }

    private void Awake() 
   {
      currentParseSystem = GetComponent<ParseSystemMain>(); 
      currentFabric = GetComponent<TopicFabric>();
      imageLoader = GetComponent <IImageLoader>();
      correctAnswers = new Dictionary<Question, bool>(); 
      Mediator = MediatorInitializer.GetInstance;
    
   }
   

   
   public void Initialize(Topic topic)
   {
       currentTopic = topic;
       
        topic.ItitializeQuestionsList(currentParseSystem, currentFabric);
        currentList = topic.QuestionsList;
        header.text = currentTopic.name;
        
      foreach (var quest in  currentList)
      {
          correctAnswers.Add (quest, false);
      }
     
      CreateNewQuestion();
       
      
   }

    public void CreateNewQuestion()
    {
             if (currentList.Count > 0)
        {
            currentQuestion = currentList[UnityEngine.Random.Range(0, currentList.Count -1)];
            currentList.Remove(currentQuestion);
             if( !string.IsNullOrEmpty( currentQuestion.imageLink))
             {
                imageLoader.LoadImage(currentQuestion.imageLink, OnLoaded);
             }
       
       question.text = currentQuestion.questionText;
       
       var  optionList = currentQuestion.optionList;
       
       List<int> num = new List<int>();
       for (int j = 0; j< optionList.Count; j++)
       {
           num.Add(j);
       }

       for (int i =0; i<optionList.Count; i++)
       {         
           int rng = num[UnityEngine.Random.Range(0, num.Count )];
           
           opttions[i].currentOption = optionList[rng];
           
           num.Remove(rng);
           opttions[i].Init();
       }
        }
        else
       {
          InitializeComponent();
           
       }
      
          
      
    }


    


    public void GetNextQuestion ( Option opt)
    {
        correctAnswers[currentQuestion] = opt.isCorrect;
           
        
        CreateNewQuestion();
    }

    
    private void OnLoaded ( Sprite img)
    {
         image.sprite = img;
    }

    public void InitializeComponent()
    {
        Mediator.RequestInitialization(this);
    }
}

public delegate void OnImageLoaded (Sprite img);
