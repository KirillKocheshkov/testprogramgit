﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileInfo : MonoBehaviour
{
    [SerializeField] private Text nick;
    [SerializeField] private Image icon;
    // Start is called before the first frame update
    private void Start()
    {
          if(GameManager.Instance.currentProfile != null)
        {
            nick.text = GameManager.Instance.currentProfile.Name ;
            icon.sprite = GameManager.Instance.currentProfile.Icon; 
        }
        
    }
    

   
}
