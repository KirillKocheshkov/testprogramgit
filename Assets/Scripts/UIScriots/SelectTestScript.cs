﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTestScript : MonoBehaviour
{
    [SerializeField] private Transform parent;
    [SerializeField] private ListContentSript prefab;

    [SerializeField] public Canvas parentCanvas;
    private List<GameObject> prefabList;


    private void OnEnable()
    {
        GenerateTopicList();
    }

    private void GenerateTopicList()
    {
        prefabList = new List<GameObject>();
        List<Topic> topics = GameManager.Instance.ListOfTopics;
        foreach (var item in topics)
        {
            int i = 0;
            ListContentSript content = Instantiate(prefab, parent);
            content.orderInList = i++;
            content.CurrentToppic = item;
            content.currentWidget = parentCanvas;
            content.OnPressedEvent += OnTopicWasChosen;

            content.Init();
            prefabList.Add(content.gameObject);

        }
    }

    private void OnTopicWasChosen()
    {
        foreach (var item in prefabList)
        {
            Destroy(item);
        }
    }
}




