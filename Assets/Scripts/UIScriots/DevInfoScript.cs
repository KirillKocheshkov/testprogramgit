﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DevInfoScript : MonoBehaviour
{
    [SerializeField] Text list;
    [SerializeField] OpenLink link;
    private DevInfo info;

    

    private void Awake()
     {
        info = GetComponent<DevInfo> ();
    }
   private void OnEnable() 
    {
         
        
        link.url = info.url;
       foreach (var name in  info.names)
       {
           list.text += name + "\n" ;
       }
         
    } 
   
    
}
