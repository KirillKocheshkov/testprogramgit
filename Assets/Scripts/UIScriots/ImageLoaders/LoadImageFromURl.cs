﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LoadImageFromURl : MonoBehaviour, IImageLoader
{

    private OnProgressChanged progressDelegate;

    public OnProgressChanged ProgressDelegate { get => progressDelegate; set => progressDelegate = value; }

    public void LoadImage(string url, OnImageLoaded delerage)
    {
      StartCoroutine (Load ( url, delerage));
      
    }
    

    private IEnumerator Load (string url, OnImageLoaded deleg)
    {
        UnityWebRequest loader = UnityWebRequestTexture.GetTexture(url);
        var loaderreq = loader.SendWebRequest();
        
        while (!loader.isDone)
        {
            if(progressDelegate != null)
            {
                progressDelegate(loaderreq.progress);
            }
           

            yield return loaderreq;
        }

        Texture2D tex = DownloadHandlerTexture.GetContent(loader);
         Sprite URLimage = Sprite.Create(tex , new Rect(0,0,tex.width, tex.height), new Vector2 (0,0));
        deleg(URLimage);
    }


}

public delegate void OnProgressChanged( float progress);

