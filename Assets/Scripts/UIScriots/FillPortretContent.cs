﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillPortretContent : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] SelectPortret prefab;
    [SerializeField ] IntroScript script;
    List<SelectPortret> portretScriptList ;
    Sprite[] images;

    private void Start()
    {
        images = Resources.LoadAll<Sprite> ("Images/ProfilePick/");
        portretScriptList = new List<SelectPortret>();
        CreatePortrets();
        script.deselect = Deselect;
        script.select = Select;
    }

    private void CreatePortrets()
    {
        foreach(Sprite image in images)
        {
            
            SelectPortret obj = Instantiate(prefab,transform);
            obj.portret.sprite = image;
            obj.script = script;
            portretScriptList.Add(obj);
        }
    }
    public void Select(Sprite s)
  {
      if(s != null)
      {
         SelectPortret currentPortrer = portretScriptList.Find(x => x.portret.sprite == s);
         currentPortrer.Select();
      }
  }
  public void Deselect(Sprite s)
  {
      if(s != null)
      {
            SelectPortret currentPortrer = portretScriptList.Find(x => x.portret.sprite == s);
            currentPortrer.Deselect();
      }
    
  }
}
