﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenLink : MonoBehaviour, IButton
{
    [HideInInspector] public string url;

    public event Action OnPressedEvent;

    public void OnButtonPressed ()
   {
      if(!string.IsNullOrEmpty(url)) Application.OpenURL(url);
      OnPressedEvent?.Invoke();
   }
}
