﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsScript : MonoBehaviour
{
     private Dictionary <Question, bool> correctAnswers;
     [SerializeField ] Text resText;
     private Topic _currentTopic;
     private int cout;
     public void Init(Dictionary <Question, bool> dic, Topic currentTopic)
     {
         correctAnswers = dic;
         _currentTopic= currentTopic;
         foreach (var res in dic)
         {
             if( res.Value)
             {
                 cout++;
             }
         }

            resText.text = cout + " / " + dic.Count;
     }
}
