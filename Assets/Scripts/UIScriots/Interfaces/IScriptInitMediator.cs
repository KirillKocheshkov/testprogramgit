﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScriptInitMediator
{
    void RequestInitialization(MonoBehaviour requestSender);
}