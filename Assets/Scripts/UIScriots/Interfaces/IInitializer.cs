﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInitializer 
{
   IScriptInitMediator Mediator {get;}
   void InitializeComponent();
}
