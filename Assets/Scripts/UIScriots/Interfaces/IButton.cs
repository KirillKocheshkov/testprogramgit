﻿using System;

public interface IButton 
{
    event Action OnPressedEvent;
}
