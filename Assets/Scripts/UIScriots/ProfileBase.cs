﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProfileBase 
{
 public abstract Sprite Icon{get;protected set;}
 public abstract string Name {get; protected set;}

  
}
