﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OnOptionsPressed : MonoBehaviour,IButton
{
    [HideInInspector] public Option currentOption;
    [SerializeField] private Text option;
     [SerializeField ] private QuestionManager managet;
     
     
     [SerializeField] ComfirmManager comfirmWidget;
     
    
    public event Action OnPressedEvent;

    public void Init ()
    {
        option.text = currentOption.text;
        
    }
    
    public void OnOptionPressed()
    {

        comfirmWidget.gameObject.SetActive(true);
        comfirmWidget.YesBut.OnPressedEvent += GetNextQuestion;

        OnPressedEvent?.Invoke();

    }
    public void GetNextQuestion()
    {
        managet.GetNextQuestion(currentOption);
         comfirmWidget.YesBut.OnPressedEvent -=GetNextQuestion;
    }

    
}
public delegate void Action ();