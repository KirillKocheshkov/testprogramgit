﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPressed : MonoBehaviour, IUIElement, IButton
{
    [SerializeField] GameObject widget;
    [SerializeField] GameObject currentWidget;
     public event Action  OnPressedEvent;
    public void OnButtonPressed ()
    {
        if(currentWidget != null) 
        {
              Hide();              
             
        }
        if(widget!= null)
        {
           Show();
        }
         
          if(OnPressedEvent!= null)
              {
                  OnPressedEvent();
                 
              }
    }
    public void CLearEvents()
    {
        OnPressedEvent = null;
    }

    public void Show()
    {
         widget.SetActive(true);
    }

    public void Hide()
    {
       currentWidget.SetActive(false);
    }
}
