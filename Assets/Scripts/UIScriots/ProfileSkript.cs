﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfileSkript : ProfileBase 
{
    private Sprite icon;
    private string _name;


    public ProfileSkript ( string name, Sprite  icon)
    {
        this.Icon = icon;
        Name = name;
    }

    public override Sprite Icon { get => icon; protected set => icon = value; }
    public override string Name { get => _name;protected set => _name = value; }



    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.currentProfile = this;
        
    }
    
    
}
