﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TouchDetecterMain : MonoBehaviour
{
   public abstract bool Touched {get; set;}
   public abstract void TouchStart();
   public abstract void TouchEnds ();

   
}
