﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnMouseTouchDetecter : TouchDetecterMain
{
    private bool touch;
    public override bool Touched { get => touch; set => touch = value; }

    public override void TouchEnds()
    {
         Touched= true;
    }

    public override void TouchStart()
    {
        Touched= true;
    }

    // Start is called before the first frame update
  

 private void Update()
 {
     if(Input.GetMouseButtonDown(0)) 
      {
           Touched = MouseOverUI(); 
      }
     if(Input.GetMouseButtonUp(0))
     {
          Touched = false;
     }
 
 }

    private bool MouseOverUI()
    {
       PointerEventData pEd = new PointerEventData(EventSystem.current);
       pEd.position = Input.mousePosition;
       List<RaycastResult> rayList = new List<RaycastResult>();
       EventSystem.current.RaycastAll (pEd, rayList);
       foreach ( var item in rayList)
       {
            if(item.gameObject.GetComponent<OnMouseTouchDetecter>() != null)
            {
                 return true;
                   
            }
       }
       return false;
    }
}
