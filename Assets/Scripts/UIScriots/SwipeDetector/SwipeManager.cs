﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeManager : SwipeManagerBase
{
    private Vector3 widgetLocation;
    Vector3 touchStart;
    TouchDetecterMain detecter;
    public float present = 0.2f;
    RectTransform transChild;
    Vector3 startPoint;
    Vector3 endPoint;
    RectTransform parentTransform;
    OnOptionsPressed[] buttons;
    bool opened;
    bool isDraging;
    bool isMoving;
    public float scrollSlowdown;

    private void Awake()
    {
        detecter = GetComponent<TouchDetecterMain>();
        parentTransform = (RectTransform)GetComponentInParent<Canvas>().transform;
        buttons = GetComponentsInChildren<OnOptionsPressed>();

        widgetLocation = transform.localPosition;
        transChild = (RectTransform)transform.Find("Options").transform;
        startPoint = transform.localPosition;

    }
    private void Start()
    {
        endPoint = new Vector3(transform.localPosition.x, GetEndPoint(), transform.localPosition.z);


    }

   

    private float GetEndPoint()
    {

        return transChild.rect.height - (parentTransform.rect.height / 2);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (detecter.Touched)
        {
            isDraging = true;
            VerticleDrag(eventData);

        }

    }
    public override void OnBeginDrag(PointerEventData eventData)
    {
        touchStart = eventData.position;
    }

    private void VerticleDrag(PointerEventData eventData)
    {

        float difference = (eventData.pressPosition.y - eventData.position.y) * (parentTransform.rect.height / Screen.height);

        Vector3 newLocation = widgetLocation - new Vector3(0, difference, 0);
        if (touchStart.y < eventData.position.y)
        {
            MoveUP(newLocation, eventData);


        }
        if (touchStart.y > eventData.position.y)
        {
            MoveDown(newLocation, eventData);


        }
        if (endPoint.y != GetEndPoint())
        {
            endPoint = new Vector3(transform.localPosition.x, GetEndPoint(), transform.localPosition.z);
            Debug.Log(endPoint);
        }

    }

    private void MoveDown(Vector3 location, PointerEventData eventData)
    {
        if (transform.localPosition.y > startPoint.y)
        {

            transform.localPosition = location;
            touchStart = eventData.position;

        }
        if (transform.localPosition.y < startPoint.y)
        {

            transform.localPosition = startPoint;
            opened = false;
        }

    }

    private void MoveUP(Vector3 location, PointerEventData eventData)
    {
        if (transform.localPosition.y < endPoint.y)
        {

            transform.localPosition = location;
            touchStart = eventData.position;
        }
        if (transform.localPosition.y > endPoint.y)
        {

            transform.localPosition = endPoint;
            opened = true;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        var leng = endPoint.y - startPoint.y;
        if (!opened && transform.localPosition != endPoint)
        {
            var end = transform.localPosition;
            float speed = Mathf.Abs((endPoint.y - end.y) / leng);
            StartCoroutine(Move(end, endPoint, speed));
            opened = true;
        }
        else if (opened && transform.localPosition != startPoint)
        {
            var end = transform.localPosition;
            float speed = Mathf.Abs((startPoint.y - end.y) / leng);
            StartCoroutine(Move(end, startPoint, speed));
            opened = false;
        }

        widgetLocation = transform.localPosition;
        touchStart = Vector3.zero;
        isDraging = false;

    }

    public void OpenMenu()
    {
        if (!isDraging && !isMoving)
        {
            if (opened)
            {
                DragDown();
            }
            else
            {
                DragUp();
            }
        }

    }
    public void DragUp()
    {

        var start = transform.localPosition;
        StartCoroutine(Move(start, endPoint, scrollSlowdown));
        opened = true;
    }

    public void DragDown()
    {
        var end = transform.localPosition;
        StartCoroutine(Move(end, startPoint, scrollSlowdown));
        opened = false;
    }

    private IEnumerator Move(Vector3 start, Vector3 end, float speed)
    {
        float t = 0f;
        while (t <= 1f)
        {

            t += Time.deltaTime / speed;
            isMoving = true;
            transform.localPosition = Vector3.Lerp(start, end, t);
            yield return null;

        }
        widgetLocation = transform.localPosition;
        isMoving = false;
    }
    private void OnDisable()
    {
        foreach (var ev in buttons)
        {
            ev.OnPressedEvent -= DragDown;
        }
    }
     private void OnEnable()
    {
        if (buttons != null)
        {
            foreach (var ev in buttons)
            {
                ev.OnPressedEvent += DragDown;
            }
        }


    }

}


