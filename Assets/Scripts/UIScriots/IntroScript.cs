﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class IntroScript : MonoBehaviour, IButton
{

    [HideInInspector] private Sprite portret;
    [SerializeField] InputField newName;
    ButtonPressed button;
   [SerializeField] Button acceptButton;
   public Action<Sprite> select;
   public Action<Sprite> deselect;
  
    UnityAction<string> onEndEdit;

    public event Action OnPressedEvent;

    public Sprite Portret 
    {
        get => portret;
        set
        {
         if(deselect!=null) deselect(portret);
            portret = value;   
         if(select!=null) select(portret);
        } 
    } 
     
     

    

    private void Awake() 
    {
        button = GetComponent <ButtonPressed> ();
        onEndEdit = CheckState;
        newName.onEndEdit.AddListener(onEndEdit);
    }


    public void OnAcceptPressed ()
    {
        
        if( !string.IsNullOrEmpty(newName.text) && portret != null)
        {
            var profile = new ProfileSkript ( newName.text, portret);
            GameManager.Instance.currentProfile = profile;
            button.OnButtonPressed();
            OnPressedEvent?.Invoke();
            
        }
        
    }
    public void CheckState( string text)
    {
        if(!string.IsNullOrEmpty(newName.text) && portret != null) acceptButton.interactable = true;
       

    }

    
}
