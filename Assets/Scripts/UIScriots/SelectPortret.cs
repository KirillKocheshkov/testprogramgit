﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectPortret : MonoBehaviour
{
  [HideInInspector] public IntroScript script;
  public Image portret;
  [SerializeField] Image backGround;

  [SerializeField] Sprite spriteSelected;
  [SerializeField] Sprite spriteDeselected;


  

  public void OnButtonPressed ()
  {
      if(portret.sprite != null)
      {
          script.Portret = portret.sprite;
          script.CheckState("");
      }
  }

  public void Select()
  {
backGround.sprite = spriteSelected;
  }

public void Deselect()
{
backGround.sprite = spriteDeselected;
}
}
