﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListContentSript : MonoBehaviour, IInitializer, IUIElement, IButton
{
    [HideInInspector] public int orderInList;

    private Topic currentToppic;
    public Text textName;

    
    public Canvas currentWidget;

    public event Action OnPressedEvent;

    public Topic CurrentToppic { get => currentToppic; set => currentToppic = value; }

    public IScriptInitMediator Mediator { get; private set; }

    public void Init()
    {
        textName.text = currentToppic.name;
        Mediator = MediatorInitializer.GetInstance;


    }

    public void OnButtonPressed()
    {


        ((MediatorInitializer)Mediator).ListContentSript = this;
        InitializeComponent();
        OnPressedEvent?.Invoke();


    }

    public void InitializeComponent()
    {
        Mediator.RequestInitialization(this);
    }

    public void Show()
    {

    }

    public void Hide()
    {
        currentWidget?.gameObject.SetActive(false);
    }
}
