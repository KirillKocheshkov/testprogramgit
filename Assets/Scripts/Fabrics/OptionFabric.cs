﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OptionFabric : MonoBehaviour
{
    public abstract List<Option> GenerateOptions (QuestionItems data) ;
    
}
