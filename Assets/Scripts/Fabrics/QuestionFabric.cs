﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionFabric : TopicFabric
{
    [SerializeField] private OptionFabric fabric;
    public override List<Question> GenerateQuestions(List<QuestionItems> data)
    {
        var listQuestions = new List<Question>();
        foreach (var qi in data)
        {
            var question = new Question()
            {
                questionID = qi.questionID,
                CorrectOption = qi.correctOption,
                questionText = qi.questionText,
                optionList = fabric.GenerateOptions(qi),
                imageLink = qi.imageLink

            };

            listQuestions.Add(question);
        }
        return listQuestions;

    }

   

}
