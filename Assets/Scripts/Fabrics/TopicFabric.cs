﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TopicFabric : MonoBehaviour
{
    public abstract List<Question> GenerateQuestions (List<QuestionItems> data);
}
