﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionFabricChild : OptionFabric
{
    public override List<Option> GenerateOptions(QuestionItems data) /// make it with array
    {
       var optionlist = new List<Option>();
       for (int i = 0; i <  data.optionsList.Count; i++)
       {
           
             var opt = new Option(data.optionsList[i].text,data.optionsList[i].wrongAnswer,data.optionsList[i].link );
             if((data.correctOption)-1 == i)
             {
                 opt.isCorrect = true;
             }
             optionlist.Add (opt);
             
       }
            
       return optionlist;



    }
}
