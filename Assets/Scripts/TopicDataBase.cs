﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "CreateTopic", menuName = "Create Test/ New Topic", order = 0)]
public class TopicDataBase : ScriptableObject
{
   [SerializeField] List<Topic> topicList;
    
   [SerializeField] private Topic currentTopic;
   private int index;

    public List<Topic> TopicList { get => topicList;  }

    public void AddTopic( )
    {
       if(topicList == null) 
       {
           topicList = new List<Topic>();
       }
       
          var topic = new Topic();
          topicList.Add(topic);
          currentTopic = topic;
          index= topicList.Count -1;
       
        
    }
     public void RemoveItem()
    {
        if(topicList == null || currentTopic == null) return;
        topicList.Remove(currentTopic);
        if (topicList.Count > 0 )currentTopic = topicList[0];
        else
        {
            currentTopic = null;
        }
        index = 0;


    }
    public void NextItem()
    {
        if(index + 1 < topicList.Count)
        {
            index ++;
             currentTopic = topicList[index];;
        }
    }
    
    public void PreviousItem()
    {
        if(index  > 0)
        {
            index --;
            currentTopic = topicList[index];
        }
    }
    public Topic GetItem(int id)
    {
        return topicList[id];
    }



}



#region ("Item)

[System.Serializable] public class Topic 
{
    [SerializeField] public string name;
    
    [SerializeField] private string fileName;
    [SerializeField,HideInInspector] private  List<Question> questionsList;
   [SerializeField,HideInInspector]  private Question currenQuestion;

    public List<Question> QuestionsList { get => questionsList;  }

    public Topic ()
    {

    }

public Topic( string name, string filePath)
{
    this.name = name;
    fileName = filePath;
}
  public void ItitializeQuestionsList(ParseSystemMain currentParseSystem,TopicFabric fabric )
    {
              
      var list =  currentParseSystem.GetQuestionsFromText(fileName);
      
      questionsList = fabric.GenerateQuestions(list);     

    }

   
    

    public Question GetQuestion (int id)
    {
        return questionsList[id];
    }
    public void FindQuestion(int id)
    {
        currenQuestion = questionsList.Find(i => i.questionID == id);
    }
    
}

public struct QuestionItems
{
 public string imageLink;

 public int questionID;
 public int correctOption;

 public string questionText;
public List<OptionsItems> optionsList;

}
public struct OptionsItems
{
    
    public string text;
    public string wrongAnswer;
    public string link;
    public OptionsItems(string opt, string wrong, string url)
    {
      text = opt; wrongAnswer = wrong; link = url;
    }

}
# endregion

#region ("Option)
[System.Serializable]
public class Option 
{
[SerializeField]   public string text;
   [SerializeField]public string wrongAnswer;
    [SerializeField]public string link;
    [SerializeField] public bool isCorrect;
    public Option( )
    {
              
    }
    public Option( string text, string incorrect, string url)
    {
        this.text = text;
         wrongAnswer = incorrect;
         link = url;
        
    }
   
}
# endregion

#region ("Question)
[System.Serializable]
public class Question
{
[SerializeField]  public bool correct;
 [SerializeField, HideInInspector] public List<Option> optionList;

[SerializeField] public string imageLink;

[SerializeField] public int questionID;
[SerializeField] public int CorrectOption;

[SerializeField] public string questionText;


public Option GetOption (int id)
{
    return optionList[id];
}
}
   #endregion

   
   