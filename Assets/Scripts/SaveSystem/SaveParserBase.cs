﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SaveParserBase 
{
    public abstract void SaveData( );
   
}

[System.Serializable]
public struct SaveData
{
    public Topic CurrentTopic;
    public List<Question> ListOfQuestions;
    
    public SaveData(Topic topic,List<Question> list)
    {
        ListOfQuestions = list?? new List<Question>();
        CurrentTopic= topic;
    }

}