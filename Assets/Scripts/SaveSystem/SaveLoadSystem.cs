﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class SaveLoadSystem 
{
    public static readonly string FILE_DIRECTORY = Application.persistentDataPath + "/Saves/";



    static SaveLoadSystem()
    {

    }
    public static string LoadFile( string fileName = "/Save.txt")
    {
        if(File.Exists(FILE_DIRECTORY + fileName))
        {
            string saveString = File.ReadAllText(FILE_DIRECTORY + fileName);
            return saveString;
        }
        return null;
    }
    public static void SaveFile(string saveInfo, string fileName = "/Save.txt")
    {
        if(!Directory.Exists(FILE_DIRECTORY) )
        {
            Directory.CreateDirectory(FILE_DIRECTORY);
        }
        File.WriteAllText( FILE_DIRECTORY + fileName, saveInfo );
    }
   
}
