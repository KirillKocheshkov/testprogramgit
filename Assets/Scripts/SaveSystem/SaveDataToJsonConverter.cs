﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveDataToJsonConverter : SaveParserBase
{
    List<Question> _listOfData;
    Topic _currentTopic;
    string fileName = "/Progress.txt";

    public SaveDataToJsonConverter(List<Question> listOfData, Topic currentTopic)
    {
       _listOfData = listOfData;
      _currentTopic = currentTopic;
    }

    public override void SaveData()
    {
        SaveData saveStruct = new SaveData(_currentTopic, _listOfData);
        
        string saveString = JsonUtility.ToJson(saveStruct);
        SaveLoadSystem.SaveFile(saveString, fileName);
        
    }
}
