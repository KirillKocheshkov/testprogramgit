﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonToDataLoader : LoadParserBase
{
     string fileName = "/Progress.txt";
    public override SaveData LoadData()
    {
       string loadedJson =  SaveLoadSystem.LoadFile(fileName);
       return  JsonUtility.FromJson<SaveData>(loadedJson);
    }
}
