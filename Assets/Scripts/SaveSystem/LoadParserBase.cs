﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LoadParserBase 
{
    public abstract SaveData LoadData( );
}
